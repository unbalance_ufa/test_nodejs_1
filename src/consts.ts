export const Consts = {
    dp_provide: 'SEQUELIZE',
    users_rep: 'USERS_REPOSITORY',
    passwords_rep: 'PASSWORDS_REPOSITORY',
    ERROR_REQUIRED_FIELDS: 'Not all required fields',
    ERROR_ACCESS_TOKEN: 'Not valid access token',
    ERROR_REFRESH_TOKEN: 'Not valid refresh token',
    ERROR_MAIL_PASSWORD: 'Not valid mail-password',
    ERROR_PASSWORD_NOT_ACTIVE: 'Password is not active. Create new password',
    ERROR_MAIL: 'Mail is busy',
    ERROR_PASSWORD: 'Not valid old password',
    ERROR_PASSWORD_DISCREDIT: 'password is discredited',
    ERROR_USER_NOT_FOUND: 'User not found',
};
