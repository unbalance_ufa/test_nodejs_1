export interface UserGetProfileInterface {
    userId?: number;
    userName?: string;
    accessToken?: number;
}
